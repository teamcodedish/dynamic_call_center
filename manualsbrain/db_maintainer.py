from scraping_interface.scraping_interface import DBMaintainer, DBMaintainerAfterDownload


class ManualDBMaintainer(DBMaintainer):

    def initial_setup(self, *args, **kwargs):
        """
        data base initial setup
        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """

    def enter_data_into_db(self, *args, **kwargs):
        """
        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass

    def close_db_connection(self, *args, **kwargs):
        """

        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass


class ManualDBMaintainerAfterDownload(DBMaintainerAfterDownload):

    def initial_setup(self, *args, **kwargs):
        """
        data base initial setup
        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """

    def enter_data_into_db(self, *args, **kwargs):
        """
        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass

    def close_db_connection(self, *args, **kwargs):
        """

        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass
