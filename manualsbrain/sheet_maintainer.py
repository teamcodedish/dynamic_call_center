from scraping_interface.scraping_interface import SheetMaintainer, \
    SheetMaintainerAfterDownload


class ManualSheetMaintainer(SheetMaintainer):

    def initial_setup(self, *args, **kwargs):
        """
        sheet base initial setup
        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """

    def enter_data_into_sheet(self, *args, **kwargs):
        """
        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass

    def close_file(self, *args, **kwargs):
        """

        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass


class ManualSheetMaintainerAfterDownload(SheetMaintainerAfterDownload):

    def initial_setup(self, *args, **kwargs):
        """
        sheet base initial setup
        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """

    def enter_data_into_sheet(self, *args, **kwargs):
        """
        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass

    def close_file(self, *args, **kwargs):
        """

        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass
