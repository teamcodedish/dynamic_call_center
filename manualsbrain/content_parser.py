from scraping_interface.scraping_interface import ContentParser


class ManualContentParser(ContentParser):

    def initial_setup(self, *args, **kwargs):
        """

        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """

    def get_brand_name(self, *args, **kwargs):
        """
        example: Andersen Windows & Doors
        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass

    def get_category_name(self, *args, **kwargs):
        """
        ex: Baby Accessories
        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass

    def get_user_manual_title(self, *args, **kwargs):
        """
        ex: AN74I
        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass

    def get_user_manual_description(self, *args, **kwargs):
        """

        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass

    def get_pdf_file_user_manual(self, *args, **kwargs):
        """
        ex: /ticwv7vBcrfJ/ticwv7vBcrfJ-2-1.pdf
        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass

    def get_user_manual_meta_title(self, *args, **kwargs):
        """
        ex: Andersen Windows & Doors AN74I Baby Accessories User Manual
        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass

    def get_user_manual_meta_description(self, *args, **kwargs):
        """
        ex: Baby care manuals and parenting free pdf instructions.
        Find the parenting user manual you need for your baby product and
        more at Instamanuals.com.

        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass

    def get_image_jpg_or_jpeg_or_png(self, *args, **kwargs):
        """
        ex: baby-accessories.jpg
        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass

    def get_category_meta_title(self, *args, **kwargs):
        """

        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass

    def get_category_meta_description(self, *args, **kwargs):
        """

        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass

    def get_brand_content(self, *args, **kwargs):
        """

        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass

    def get_brand_meta_title(self, *args, **kwargs):
        """

        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass

    def get_brand_meta_description(self, *args, **kwargs):
        """

        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass
