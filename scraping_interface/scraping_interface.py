from abc import ABC, abstractmethod


class ContentParser(ABC):

    @abstractmethod
    def initial_setup(self, *args, **kwargs):
        """

        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """

    @abstractmethod
    def get_brand_name(self, *args, **kwargs):
        """
        example: Andersen Windows & Doors
        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass

    @abstractmethod
    def get_category_name(self, *args, **kwargs):
        """
        ex: Baby Accessories
        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass

    @abstractmethod
    def get_user_manual_title(self, *args, **kwargs):
        """
        ex: AN74I
        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass

    @abstractmethod
    def get_user_manual_description(self, *args, **kwargs):
        """

        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass

    @abstractmethod
    def get_pdf_file_user_manual(self, *args, **kwargs):
        """
        ex: /ticwv7vBcrfJ/ticwv7vBcrfJ-2-1.pdf
        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass

    @abstractmethod
    def get_user_manual_meta_title(self, *args, **kwargs):
        """
        ex: Andersen Windows & Doors AN74I Baby Accessories User Manual
        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass

    @abstractmethod
    def get_user_manual_meta_description(self, *args, **kwargs):
        """
        ex: Baby care manuals and parenting free pdf instructions.
        Find the parenting user manual you need for your baby product and
        more at Instamanuals.com.

        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass

    @abstractmethod
    def get_image_jpg_or_jpeg_or_png(self, *args, **kwargs):
        """
        ex: baby-accessories.jpg
        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass

    @abstractmethod
    def get_category_meta_title(self, *args, **kwargs):
        """

        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass

    @abstractmethod
    def get_category_meta_description(self, *args, **kwargs):
        """

        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass

    @abstractmethod
    def get_brand_content(self, *args, **kwargs):
        """

        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass

    @abstractmethod
    def get_brand_meta_title(self, *args, **kwargs):
        """

        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass

    @abstractmethod
    def get_brand_meta_description(self, *args, **kwargs):
        """

        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass


class DBMaintainer(ABC):

    @abstractmethod
    def initial_setup(self, *args, **kwargs):
        """
        data base initial setup
        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """

    @abstractmethod
    def enter_data_into_db(self, *args, **kwargs):
        """
        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass

    @abstractmethod
    def close_db_connection(self, *args, **kwargs):
        """

        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass


class SheetMaintainer(ABC):

    @abstractmethod
    def initial_setup(self, *args, **kwargs):
        """
        sheet base initial setup
        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """

    @abstractmethod
    def enter_data_into_sheet(self, *args, **kwargs):
        """
        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass

    @abstractmethod
    def close_file(self, *args, **kwargs):
        """

        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass


class ManualDownloader(ABC):

    @abstractmethod
    def download_manual(self, *args, **kwargs):
        """
        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass


class DBMaintainerAfterDownload(ABC):

    @abstractmethod
    def initial_setup(self, *args, **kwargs):
        """
        data base initial setup
        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """

    @abstractmethod
    def enter_data_into_db(self, *args, **kwargs):
        """
        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass

    @abstractmethod
    def close_db_connection(self, *args, **kwargs):
        """

        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass


class SheetMaintainerAfterDownload(ABC):

    @abstractmethod
    def initial_setup(self, *args, **kwargs):
        """
        sheet base initial setup
        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """

    @abstractmethod
    def enter_data_into_sheet(self, *args, **kwargs):
        """
        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass

    @abstractmethod
    def close_file(self, *args, **kwargs):
        """

        :param args:
        :type args:
        :param kwargs:
        :type kwargs:
        :return:
        :rtype:
        """
        pass
