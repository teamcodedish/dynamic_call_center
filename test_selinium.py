import os
import time
from urllib.parse import urlparse

import requests
from PyPDF2 import PdfFileMerger, PdfFileWriter, PdfFileReader
from bs4 import BeautifulSoup
from pyhtml2pdf import converter


def pdf_to_html():
    """
    pdf2htmlEX   --zoom 1.3 --no-drm 1000000 UMKL100.pdf

    :return:
    :rtype:
    """
    print("To Do")


def html_to_pdf_generator(url, storage_path="static_files"):
    """

    :param url:
    :type url:
    :return:
    :rtype:
    """
    page = requests.get(url)
    o = urlparse(page.url)
    folder_name = o.path.replace(".html", "").split("/")[-1]

    content = page.content
    page.close()

    soup = BeautifulSoup(content, 'html.parser')
    page_pagination = soup.find("form", class_="pages_form").get_text().split()
    of_index = page_pagination.index("of")
    last_page_number = int(page_pagination[of_index + 1])
    # last_page_number = 10

    result = soup.find("div", class_='page-doc')
    result = str(result).replace("url(//static-data2.manualslib.com",
                                 "url(http://static-data2.manualslib.com")
    with open("header.html") as f:
        header_content = f.read()

    with open("footer.html") as f:
        footer_content = f.read()

    folder_path = "{}/{}".format(storage_path, folder_name)
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)

    file_path = "{}/new_page.html".format(folder_path)
    pdf_file_path = "{}/sample.pdf".format(folder_path)

    with open(file_path, "w", encoding='utf-8') as f:
        f.write(header_content)
        for i in range(1, last_page_number + 1):
            url = url_format.format(str(i))
            print(url)
            time.sleep(1)
            page = requests.get(url)
            content = page.content
            page.close()
            soup = BeautifulSoup(content, 'html.parser')
            result = soup.find("div", class_='page-doc')
            anchor_list = result.find_all("a")
            for anchor in anchor_list:
                if anchor.has_attr('data-caption'):
                    del anchor["href"]
            result = str(result).replace("url(//static-data2.manualslib.com",
                                         "url(http://static-data2.manualslib.com")
            result = result.replace("div.pdf", "div.pdf_{}".format(i))
            result = result.replace('class="pdf"',
                                    'class="pdf pdf_{}"'.format(i))
            result = result.replace('<a class="zoom start_zoom" href="#">', '')
            f.write(result)
        f.write(footer_content)
        convert_html_to_pdf(file_path, pdf_file_path)


def convert_html_to_pdf(file_path, pdf_file_path):
    path = os.path.abspath(file_path)
    converter.convert(f'file:///{path}', pdf_file_path)
    # pdfkit.from_file(path, pdf_file_path)


def pdf_merger(pdf_file_list):
    merger = PdfFileMerger()

    for pdf in pdf_file_list:
        merger.append(pdf)

    merger.write("result.pdf")
    merger.close()

    pages_to_delete = pdfs = [i for i in
                              range(1, 33,
                                    2)]  # page numbering starts from 0
    infile = PdfFileReader('result.pdf', 'rb')
    output = PdfFileWriter()

    for i in range(infile.getNumPages()):
        if i not in pages_to_delete:
            p = infile.getPage(i)
            output.addPage(p)

    with open('newfile.pdf', 'wb') as f:
        output.write(f)


if __name__ == "__main__":
    url_format = "https://www.manualslib.com/manual/353953/Acer-Travelmate-C100-Series.html?page={}#manual"
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:86.0) Gecko/20100101 Firefox/86.0",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
        "Accept-Language": "en-US,en;q=0.5",
        "Upgrade-Insecure-Requests": "1",
        "referrer": "https://www.manualslib.com/manual/353953/Acer-Travelmate-C100-Series.html",
    }
    url = url_format.format("1")
    session = requests.Session()
    html_to_pdf_generator(url)
    # pdf_file_list = ['sample.pdf'.format(str(i)) for i in range(1, 33 + 1)]
    # pdf_merger(pdf_file_list)

    # html_path = "static_files/Acer-Travelmate-C100-Series/new_page.html"
    # pdf_path = "static_files/Acer-Travelmate-C100-Series/sample.pdf"
    #
    # convert_html_to_pdf(html_path,pdf_path )
